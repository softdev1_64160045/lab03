/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author chana
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayerBY_O_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWinNoPlayerBY_X_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.CheckWin(table, currentPlayer));
    }

    @Test
    public void testCheckWininRow2BY_O_output_true() {
        String[][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininRow2BY_X_output_true() {
        String[][] table = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }

    @Test
    public void testCheckWininRow1BY_X_output_true() {
        String[][] table = {{"X", "X", "X"}, {"-", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininRow1BY_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }

    @Test
    public void testCheckWininRow3BY_X_output_true() {
        String[][] table = {{"-", "O", "O"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininRow3BY_O_output_true() {
        String[][] table = {{"-", "X", "X"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
   
    @Test
    public void testCheckWininCol1BY_X_output_true() {
        String[][] table = {{"X", "-", "-"}, {"X", "O", "O"}, {"X", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininCol1BY_O_output_true() {
        String[][] table = {{"O", "-", "-"}, {"O", "X", "X"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininCol2BY_X_output_true() {
        String[][] table = {{"O", "X", "-"}, {"X", "X", "O"}, {"O", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininCol2BY_O_output_true() {
        String[][] table = {{"O", "O", "-"}, {"X", "O", "X"}, {"O", "O", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
     @Test
    public void testCheckWininCol3BY_X_output_true() {
        String[][] table = {{"O", "-", "X"}, {"X", "-", "X"}, {"O", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininCol3BY_O_output_true() {
        String[][] table = {{"X", "-", "O"}, {"X", "-", "O"}, {"X", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininDrawBY_X_output_true() {
        String[][] table = {{"X", "O", "O"}, {"O", "X", "X"}, {"X", "O", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininX1BY_X_output_true() {
        String[][] table = {{"X", "-", "O"}, {"-", "X", "-"}, {"O", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininX1BY_O_output_true() {
        String[][] table = {{"O", "-", "X"}, {"-", "O", "-"}, {"X", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininX2BY_X_output_true() {
        String[][] table = {{"-", "O", "X"}, {"O", "X", "-"}, {"X", "O", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
    
    @Test
    public void testCheckWininX2BY_O_output_true() {
        String[][] table = {{"-", "X", "O"}, {"X", "O", "-"}, {"O", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.CheckWin(table, currentPlayer));
    }
}
